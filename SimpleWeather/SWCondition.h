//
//  SWCondition.h
//  SimpleWeather
//
//  Created by  James on 14-12-11.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import <Mantle.h>

@interface SWCondition : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDate *sunrise;
@property (nonatomic, strong) NSDate *sunset;

@property (nonatomic, strong) NSNumber *humidity;
@property (nonatomic, strong) NSNumber *temperature;
@property (nonatomic, strong) NSNumber *tempHigh;
@property (nonatomic, strong) NSNumber *tempLow;
@property (nonatomic, strong) NSNumber *windBearing;
@property (nonatomic, strong) NSNumber *windSpeed;

@property (nonatomic, strong) NSString *locationName;
@property (nonatomic, strong) NSString *conditionDescription;
@property (nonatomic, strong) NSString *condition;
@property (nonatomic, strong) NSString *iconImageName;

- (NSString *)imageName;

@end
