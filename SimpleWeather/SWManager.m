//
//  SWManager.m
//  SimpleWeather
//
//  Created by  James on 14-12-11.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import "SWManager.h"
#import "SWClient.h"
#import "SWCondition.h"

#import <ReactiveCocoa.h>
#import <TSMessage.h>

@interface SWManager ()

//@property (nonatomic, strong, readwrite) SWCondition *currentCondition;
//@property (nonatomic, strong, readwrite) NSArray *hourlyForecast;
//@property (nonatomic, strong, readwrite) NSArray *dailyForecast;

@property (nonatomic, strong) SWClient *client;

@end

@implementation SWManager

#pragma mark - SETTER & GETTER

+ (SWManager *)sharedManager
{
    static id sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (id)init
{
    if (self = [super init])
    {
        _client = [[SWClient alloc] init];
    }
    return self;
}

- (RACSignal *)updateCurrentConditions
{
    __block SWManager *weakSelf = self;
    return [[_client fetchCurrentConditionsForCity:@"Beijing,CN"] doNext:^(SWCondition *condition){
        weakSelf.currentCondition = condition;
    }];
}

- (RACSignal *)updateHourlyForecast
{
    __block SWManager *weakSelf = self;
    return [[_client fetchHourlyForecastForCity:@"Beijing,CN"] doNext:^(NSArray *conditions){
        weakSelf.hourlyForecast = conditions;
    }];
}

- (RACSignal *)updateDailyForecast
{
    __block SWManager *weakSelf = self;
    return [[_client fetchDailyForecastForCity:@"Beijing,CN"] doNext:^(NSArray *conditions){
        weakSelf.dailyForecast = conditions;
    }];
}
@end
