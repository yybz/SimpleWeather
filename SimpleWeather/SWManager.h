//
//  SWManager.h
//  SimpleWeather
//
//  Created by  James on 14-12-11.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SWCondition,RACSignal;
@interface SWManager : NSObject

+ (SWManager *)sharedManager;

@property (nonatomic, strong) SWCondition *currentCondition;
@property (nonatomic, strong) NSArray *hourlyForecast;
@property (nonatomic, strong) NSArray *dailyForecast;

- (RACSignal *)updateCurrentConditions;
- (RACSignal *)updateHourlyForecast;
- (RACSignal *)updateDailyForecast;

@end
