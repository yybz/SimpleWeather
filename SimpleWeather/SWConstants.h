//
//  SWConstants.h
//  SimpleWeather
//
//  Created by  James on 14-12-11.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define OpenWeatherServer @"http://api.openweathermap.org/data/2.5/"
#define CurrentWeatherURL [NSString stringWithFormat:@"%@weather?q=",OpenWeatherServer]
#define HourlyForecastURL [NSString stringWithFormat:@"%@forecast?q=",OpenWeatherServer]
#define DailyForcastURL [NSString stringWithFormat:@"%@forecast/daily?q=",OpenWeatherServer]

@interface SWConstants : NSObject

@end
