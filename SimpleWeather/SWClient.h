//
//  SWClient.h
//  SimpleWeather
//
//  Created by  James on 14-12-12.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@interface SWClient : NSObject

- (RACSignal *)fetchJSONFromUrlString:(NSString *)urlString;
- (RACSignal *)fetchCurrentConditionsForCity:(NSString *)cityName;
- (RACSignal *)fetchHourlyForecastForCity:(NSString *)cityName;
- (RACSignal *)fetchDailyForecastForCity:(NSString *)cityName;

@end
