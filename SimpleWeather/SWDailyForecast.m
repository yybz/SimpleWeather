//
//  SWDailyForecast.m
//  SimpleWeather
//
//  Created by  James on 14-12-11.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import "SWDailyForecast.h"

@implementation SWDailyForecast

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    NSMutableDictionary *paths = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    // 2
    paths[@"tempHigh"] = @"temp.max";
    paths[@"tempLow"] = @"temp.min";
    // 3
    return paths;
}

@end
