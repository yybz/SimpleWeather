//
//  SWClient.m
//  SimpleWeather
//
//  Created by  James on 14-12-12.
//  Copyright (c) 2014年 Siemens. All rights reserved.
//

#import "SWClient.h"
#import "SWCondition.h"
#import "SWDailyForecast.h"

#import "SWConstants.h"

#import <ReactiveCocoa.h>

@interface SWClient ()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation SWClient

- (id)init
{
    self = [super init];
    if (self)
    {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

#pragma mark - Signals

- (RACSignal *)fetchJSONFromUrlString:(NSString *)urlString
{
    NSLog(@"fetch..%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    RACSignal *fetchSignal;
    
    __block SWClient *weakSelf = self;
    fetchSignal = [[RACSignal createSignal:^RACDisposable*(id<RACSubscriber> subscriber){
        NSURLSessionDataTask *dataTask = [weakSelf.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response,NSError *error){
            if (!error)
            {
                id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                [subscriber sendNext:json];
            }
            else
            {
                [subscriber sendError:error];
            }
            [subscriber sendCompleted];
        }];
        [dataTask resume];
        return [RACDisposable disposableWithBlock:^{
            [dataTask cancel];
        }];
    }] doError:^(NSError *error){
        NSLog(@"%@",error);
    }];
    
    return fetchSignal;
}

- (RACSignal *)fetchCurrentConditionsForCity:(NSString *)cityName
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",CurrentWeatherURL,cityName];
    return [[self fetchJSONFromUrlString:urlString] map:^(NSDictionary *json){
        id result = [MTLJSONAdapter modelOfClass:[SWCondition class] fromJSONDictionary:json error:nil];
        return result;
    }];
}

- (RACSignal *)fetchHourlyForecastForCity:(NSString *)cityName
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@&mode=json",HourlyForecastURL,cityName];
    return [[self fetchJSONFromUrlString:urlString] map:^(NSDictionary *json){
        RACSequence *list = [json[@"list"] rac_sequence];
        return [[list map:^(NSDictionary *item) {
            return [MTLJSONAdapter modelOfClass:[SWCondition class] fromJSONDictionary:item error:nil];
        }] array];
    }];
}

- (RACSignal *)fetchDailyForecastForCity:(NSString *)cityName
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@&mode=json&units=metric&cnt=7",DailyForcastURL,cityName];
    
    return [[self fetchJSONFromUrlString:urlString] map:^(NSDictionary *json) {
        
        RACSequence *list = [json[@"list"] rac_sequence];
        return [[list map:^(NSDictionary *item) {
            return [MTLJSONAdapter modelOfClass:[SWDailyForecast class] fromJSONDictionary:item error:nil];
        }] array];
    }];
}
@end
